﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace MyGame.EntityDefinition
{
	public class MovingEntity : Entity, IMovingEntity
	{
		private Vector2 _minSpeed = new Vector2(0.01f, 0.01f);

		#region Constructors

        public MovingEntity(Texture2D texture, Rectangle location, Vector2 speed, float maxSpeed)
			: base(texture, location)
		{
			Location = location;
			Speed = speed;
			MaxSpeed = maxSpeed;
		}

		public MovingEntity(Texture2D texture, Rectangle location, Vector2 speed)
			: this (texture, location, speed, float.NegativeInfinity)
		{
		}

		public MovingEntity(Texture2D texture, Rectangle location)
			: this (texture, location, Vector2.Zero, float.NegativeInfinity)
		{ 
		}

		public MovingEntity(Texture2D texture)
			: this(texture, Rectangle.Empty, Vector2.Zero, float.NegativeInfinity)
		{
		}

		#endregion Constructors

		public Vector2 Speed { get; private set; }

		public float MaxSpeed { get; private set; }

		public float speed;
		public void AdjustMaxSpeed(float percentage)
		{
			MaxSpeed *= percentage;
		}

		public void AdjustSpeed(Vector2 adjustmentVector)
		{
			if ((adjustmentVector + Speed).Length() <= MaxSpeed)
			{
				Speed += adjustmentVector;
				return;
			}

			Speed = Vector2.Normalize(Speed + adjustmentVector) * MaxSpeed;
		}

		public void SetSpeed(Vector2 newSpeed)
		{
			if (newSpeed.Length() <= MaxSpeed)
			{
				Speed = newSpeed;
				return;
			}

			Speed = Vector2.Normalize(newSpeed) * MaxSpeed;
		}

		public void UpdateLocation(float elapsedTime, IEnumerable<ICollidable> collidables)
		{
			foreach (var collidable in collidables)
			{
				var self = this as ICollidable;
				Speed = self.CollisionActor.Collide(self, collidable, Speed);
			}

			Location = new Rectangle(
				Location.X + (int)(elapsedTime * Speed.X),
				Location.Y + (int)(elapsedTime * Speed.Y),
				Location.Width,
				Location.Height);
		}

		public void SlowDown()
		{
			Speed *= 0.85f;
			if (Speed.Length() < _minSpeed.Length())
			{
				Speed = Vector2.Zero;
			}
		}

		public void Stop()
		{
			Speed = Vector2.Zero;
		}
	}
}