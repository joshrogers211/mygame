﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyGame.EntityDefinition
{
    public interface IEntity
    {
        //The entity's location
        Rectangle Location { get; set; }

        Texture2D Texture { get; }
    }
}
