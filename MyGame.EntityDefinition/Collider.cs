﻿using Microsoft.Xna.Framework;

namespace MyGame.EntityDefinition
{
    public class Collider
    {
        public Vector2 Collide<T1, T2>(T1 c1, T2 c2, Vector2 speed)
			where T1 : ICollidable
			where T2 : ICollidable
		{
			if (c1 is null)
			{
				return speed;
			}

            if (c1.GetType() == c2.GetType())
			{
				return speed;
			}
			else
			{
				var collisionSide = GetCollisionSide(c1, c2);
				if (collisionSide != CollisionSide.None)
				{
					if (collisionSide == CollisionSide.Right)
					{
						if (speed.X > 0f)
						{
							return new Vector2(0, speed.Y);
						}
					}
				}
			}
			return speed;
		}

        private CollisionSide GetCollisionSide<T1, T2>(T1 c1, T2 c2)
			where T1 : ICollidable
			where T2 : ICollidable
		{
            if ((c1.Location.Right > c2.Location.Left) && (c1.Location.Top <= c2.Location.Bottom) && (c1.Location.Bottom >= c2.Location.Top))
            {
                return CollisionSide.Right;
            }
            return CollisionSide.None;
        }
    }
}
