﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace MyGame.EntityDefinition
{
    public interface IMovingEntity : IEntity
    {
        //The entity's speed, in units per second
        Vector2 Speed { get; }

        //The maximum magnitude of the entity's speed (Speed.Length() <= MaxSpeed)
        float MaxSpeed { get; }

        //Adjusts the maximum speed by a percentage
        void AdjustMaxSpeed(float percentage);

        //Adjusts the x and y portions of the speed using a vector, up to the max speed (if specified)
        void AdjustSpeed(Vector2 adjustmentVector);

        //Takes elapsed time, and uses location and speed to set the new location
        void UpdateLocation(float elapsedTime, IEnumerable<ICollidable> collidables);

        void SlowDown();

        void Stop();
    }
}
