﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyGame.EntityDefinition
{
    public class Entity : IEntity
    {
        public Entity(Texture2D texture, Rectangle location)
        {
            Texture = texture;
            Location = location;
        }

        public Entity(Texture2D texture)
            : this (texture, Rectangle.Empty)
        {
        }

        public Texture2D Texture { get; }

        public Rectangle Location { get; set; }
    }
}