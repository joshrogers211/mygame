﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyGame.EntityDefinition
{
    public interface ICollidable : IEntity
    { 
        bool Collidable { get; }

        Collider CollisionActor { get; }
    }

    public enum CollisionSide
    {
        Top,
        Bottom,
        Left,
        Right,
        None,
    }
}