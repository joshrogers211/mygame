﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MyGame.Creatures;
using MyGame.EntityDefinition;
using MyGame.Graphics;
using MyGame.Localization;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace MyGame.Core
{
    public class Game1 : Game
    {
        private GraphicsEngine _engine;
        private List<IMovingEntity> _movingEntites;
        private List<IEntity> _allEntities;
        private Player _player;
        private double _lastTickTime;

        public Game1()
        {
            _engine = new GraphicsEngine(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            _lastTickTime = 0;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _player = new Player(Content.Load<Texture2D>("sadlad"), new Rectangle(50, 50, 50, 50), Vector2.Zero, 0.4f, 50, 50, true);
            _movingEntites = new List<IMovingEntity>
            {
                _player,
                new ColliderMan(Content.Load<Texture2D>("sadlad"), new Rectangle(200, 200, 50, 50))
            };

            _allEntities = new List<IEntity>();

            _allEntities.AddRange(_movingEntites);
            
            _engine.LoadContent(_allEntities);

            // TODO: use this.Content to load your game content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            bool hasMoved = false;

            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                _player.AdjustSpeed(new Vector2(-0.02f, 0));
                hasMoved = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                _player.AdjustSpeed(new Vector2(0, -0.02f));
                hasMoved = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                _player.AdjustSpeed(new Vector2(0.02f, 0));
                hasMoved = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                _player.AdjustSpeed(new Vector2(0, 0.02f));
                hasMoved = true;
            }
            if (!hasMoved)
            {
                _player.Stop();
            }

            var collidables = _allEntities.OfType<ICollidable>().ToList();
            _movingEntites.ForEach(x => x.UpdateLocation((float)(gameTime.ElapsedGameTime.TotalMilliseconds - _lastTickTime), collidables));

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            _engine.Draw(gameTime);
            base.Draw(gameTime);
        }
    }
}
