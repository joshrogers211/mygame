﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MyGame.EntityDefinition;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyGame.Creatures
{
    public class ColliderMan : MovingEntity, ICollidable
    {
        public ColliderMan(Texture2D tex, Rectangle location)
            : base(tex, location)
        {
            CollisionActor = new Collider();
        }

        public bool Collidable => true;

        public Collider CollisionActor { get; }
    }
}
