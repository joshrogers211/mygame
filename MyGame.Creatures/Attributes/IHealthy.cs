﻿namespace MyGame.Creatures
{
    public interface IHealthy
    {
        int Health { get; }

        int MaxHealth { get; }

        void TakeDamage(int damage);

        void Heal(int toHeal);

        void Kill();
    }
}
