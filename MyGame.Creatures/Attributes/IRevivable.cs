﻿namespace MyGame.Creatures.Attributes
{
    public interface IRevivable
    {
        bool CanBeRevived { get; }
    }
}
