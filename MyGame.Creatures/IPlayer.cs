﻿using MyGame.Creatures.Attributes;
using MyGame.Creatures.Statuses;
using MyGame.EntityDefinition;

namespace MyGame.Creatures
{
    public interface IPlayer : IHealthy, IRevivable, IEffectable, ICollidable
    {
    }
}