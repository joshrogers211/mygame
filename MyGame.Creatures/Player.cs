﻿using System.Collections.Generic;
using System;

using MyGame.Creatures.Statuses;
using MyGame.EntityDefinition;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyGame.Creatures
{
    public class Player : MovingEntity, IPlayer
    {
        private static readonly List<StatusEffect> _validStatuses = new List<StatusEffect>
        {
            StatusEffect.CantHeal,
            StatusEffect.Immune,
            StatusEffect.Poison
        };

        public Player(Texture2D texture, Rectangle location, Vector2 speed, float maxSpeed, int maxHealth, int health, bool canBeRevived = true)
            : base(texture, location, speed, maxSpeed)
        {
            Collidable = true;
            MaxHealth = maxHealth;
            Health = health;
            CanBeRevived = canBeRevived;
            Actor = new EffectActor(_validStatuses, new Action<int>(TakeDamage), true, true, false);
            CollisionActor = new Collider();
        }

        public int Health { get; private set; }

        public int MaxHealth { get; private set; }

        public bool CanBeRevived { get; private set; }

        public EffectActor Actor { get; }

        public bool Collidable { get; private set; }

        public Collider CollisionActor { get; }

        public void Heal(int toHeal)
        {
            if (Actor.CanHeal)
            {
                Health = Math.Min(Health + toHeal, MaxHealth);
            }
        }

        public void TakeDamage(int damage)
        {
            if (!Actor.Immune)
            {
                Health -= damage;
            }
        }

        public void Kill()
        {
            if (!Actor.Immune)
            {
                Health = 0;
            }
        }

        public void StartTurn()
        {
            var endedEffects = Actor.TickTurn();
        }
    }
}