﻿namespace MyGame.Creatures.Statuses
{
    public interface IEffectable
    {
        EffectActor Actor { get; }
    }
}
