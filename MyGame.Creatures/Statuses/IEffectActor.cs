﻿using System.Collections.Generic;

namespace MyGame.Creatures.Statuses
{
    public interface IEffectActor
    {
        List<StatusEffect> ValidEffects { get; }

        Dictionary<StatusEffect, StatusInfo> CurrentStatuses { get; set; }

        bool CanHeal { get; }

        bool CanRevive { get; }

        bool TeamSwapped { get; }

        int ReducedHp { get; }

        bool Immune { get; }

        float DamageMultiplier { get; }

        bool Poisoned { get; }

        void ApplyEffect(StatusInfo statusInfo);

        void ClearStatuses();

        List<StatusEffect> TickTurn();
    }
}
