﻿namespace MyGame.Creatures.Statuses
{
    public struct StatusInfo
    {
        public readonly StatusEffect Effect;
        public readonly int Duration;
        public readonly float Value;

        public StatusInfo(StatusEffect effect, int duration = 0, float value = 0)
        {
            Effect = effect;
            Duration = duration;
            Value = value;
        }

        public static StatusInfo ReduceTurnsLeft(StatusInfo info, int turns = 1)
        {
            return new StatusInfo(info.Effect, info.Duration - turns, info.Value);
        }

        public static StatusInfo ReduceValue(StatusInfo info, float value = 1)
        {
            return new StatusInfo(info.Effect, info.Duration, info.Value - value);
        }

        public static StatusInfo ReduceTurnsAndValue(StatusInfo info, int turns = 1, float value = 1)
        {
            return new StatusInfo(info.Effect, info.Duration - turns, info.Value - value);
        }
    }
}
