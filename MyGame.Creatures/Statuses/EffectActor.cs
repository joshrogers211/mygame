﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyGame.Creatures.Statuses
{
    public class EffectActor : IEffectActor
    {
        private readonly Action<int> _dealDamage;
        private readonly bool _defaultCanRevive;
        private readonly bool _defaultCanHeal;
        private readonly bool _defaultImmune;

        public EffectActor(List<StatusEffect> validEffects)
            : this(validEffects, null, false, true, false)
        {
        }

        public EffectActor(List<StatusEffect> validEffects, Action<int> damageFunc, bool canHeal, bool canRevive, bool immune)
        {
            ValidEffects = validEffects;
            _dealDamage = damageFunc;
            _defaultCanRevive = canRevive;
            _defaultCanHeal = canHeal;
            _defaultImmune = immune;

            SetDefaults();
        }

        private void SetDefaults()
        {
            CanRevive = _defaultCanRevive;
            CanHeal = _defaultCanHeal;
            Immune = _defaultImmune;
            Poisoned = false;
            DamageMultiplier = 1.0f;
            TeamSwapped = false;
            ReducedHp = 0;
        }

        public List<StatusEffect> ValidEffects { get; }

        public Dictionary<StatusEffect, StatusInfo> CurrentStatuses { get; set; }

        public bool CanRevive { get; private set; }

        public bool CanHeal { get; private set; }

        public bool Immune { get; private set; }

        public bool Poisoned { get; private set; }

        public float DamageMultiplier { get; private set; }

        public bool TeamSwapped { get; private set; }

        public int ReducedHp { get; private set; }

        public void ApplyEffect(StatusInfo statusInfo)
        {
            if (!ValidEffects.Contains(statusInfo.Effect))
            {
                return;
            }

            if (!CurrentStatuses.ContainsKey(statusInfo.Effect))
            {
                CurrentStatuses.Add(statusInfo.Effect, statusInfo);
            }

            var hasTurnsLeft = statusInfo.Duration > 0;

            switch (statusInfo.Effect)
            {
                case StatusEffect.CantHeal:
                    CanHeal = hasTurnsLeft;
                    break;

                case StatusEffect.CantRevive:
                    CanRevive = hasTurnsLeft;
                    break;

                case StatusEffect.SwapTeam:
                    TeamSwapped = hasTurnsLeft;
                    break;

                case StatusEffect.ReduceMaxHp:
                    if (hasTurnsLeft)
                    {
                        ReducedHp = Convert.ToInt32(Math.Floor(statusInfo.Value));
                    }
                    else
                    {
                        ReducedHp = 0;
                    }
                    break;

                case StatusEffect.Immune:
                    Immune = hasTurnsLeft;
                    break;

                case StatusEffect.ExtraDamage:
                    if (hasTurnsLeft)
                    {
                        DamageMultiplier = statusInfo.Value;
                    }
                    else
                    {
                        DamageMultiplier = 1.0f;
                    }
                    break;

                case StatusEffect.Poison:
                    _dealDamage?.Invoke(Convert.ToInt32(Math.Floor(statusInfo.Value)));
                    Poisoned = hasTurnsLeft;
                    break;
            }
        }

        private Dictionary<StatusEffect, Type> dict;

        public void ClearStatuses()
        {
            SetDefaults();
            CurrentStatuses.Clear();
        }

        private void ClearStatuses(List<StatusEffect> toClear)
        {
            //Might implement this later. Have to figure out how to set the defaults for chosen statuses in a non-shite way.
            throw new NotImplementedException();
        }

        public List<StatusEffect> TickTurn()
        {
            var endedEffects = new List<StatusEffect>();

            foreach (var key in CurrentStatuses.Keys)
            {
                CurrentStatuses[key] = StatusInfo.ReduceTurnsLeft(CurrentStatuses[key]);

                ApplyEffect(CurrentStatuses[key]);

                if (CurrentStatuses[key].Duration <= 0)
                {
                    endedEffects.Add(CurrentStatuses[key].Effect);
                }
            }

            return endedEffects;
        }
    }
}
