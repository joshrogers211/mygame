﻿namespace MyGame.Creatures.Statuses
{
    public enum StatusEffect
    {
        CantHeal,
        CantRevive,
        SwapTeam,
        ReduceMaxHp,
        Immune,
        ExtraDamage,
        Poison,
    }
}
