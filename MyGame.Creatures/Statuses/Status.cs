﻿using System;
using System.Linq;

namespace MyGame.Creatures.Statuses
{
    public static class Status
    {
        public static bool ApplyEffect<T>(T target, StatusInfo statusInfo) where T : IEffectable
        {
            var possibleEffects = target.Actor.ValidEffects;

            if (possibleEffects.Contains(statusInfo.Effect))
            {
                target.Actor.ApplyEffect(statusInfo);
                return true;
            }

            return false;
        }
    }
}
