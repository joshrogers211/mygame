﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MyGame.EntityDefinition;
using System.Collections.Generic;

namespace MyGame.Graphics
{
    public class GraphicsEngine
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private List<IEntity> _entities;
        private Game _game;

        public GraphicsEngine(Game game)
        {
            _game = game;
            _graphics = new GraphicsDeviceManager(game);
            _entities = new List<IEntity>();
        }

        public void LoadContent(IEnumerable<IEntity> entities)
        {
            _spriteBatch = new SpriteBatch(_game.GraphicsDevice);
            _entities.AddRange(entities);

            // TODO: use this.Content to load your game content here
        }

        public void Draw(GameTime gameTime)
        {
            _game.GraphicsDevice.Clear(Color.LightGray);

            _spriteBatch.Begin();
            _entities.ForEach(e => _spriteBatch.Draw(e.Texture, e.Location, Color.White));
            _spriteBatch.End();

            // TODO: Add your drawing code here
        }
    }
}